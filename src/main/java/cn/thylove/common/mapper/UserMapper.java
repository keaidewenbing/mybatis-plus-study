package cn.thylove.common.mapper;

import cn.thylove.common.model.sys.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author: dsq
 * @data: 2024/1/28 19:13
 * @description: TODO
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
