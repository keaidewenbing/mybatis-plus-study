package cn.thylove.common.controller;
import cn.thylove.common.entity.MyPage;
import cn.thylove.common.entity.PageResult;
import cn.thylove.common.entity.Result;
import cn.thylove.common.entity.co.UserCO;
import cn.thylove.common.entity.po.UserPO;
import cn.thylove.common.entity.vo.UserVO;
import cn.thylove.common.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: dsq
 * @data: 2024/1/28 19:16
 * @description: user
 */
@RestController
@RequestMapping("/user")
public class UserController {
    private UserService service;
    @Autowired
    public void setUserService(UserService service) {
        this.service = service;
    }

    @PostMapping("/page")
    public PageResult<UserVO> page(@RequestBody MyPage<UserCO> co) {
        return service.page(co);
    }
    @PostMapping("/save")
    public Result<UserVO> save(@RequestBody UserPO po) {
        return service.saveUser(po);
    }
}
