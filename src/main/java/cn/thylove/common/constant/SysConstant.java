package cn.thylove.common.constant;

/**
 * @author: dsq
 * @data: 2024/1/28 18:02
 * @description: 系统常量
 */
public class SysConstant {
  // SnowFlake
  public static final Long DATA_CENTER_USER_ID = 1L;
  public static final Long MACHINE_USER_ID = 1L;
}
