package cn.thylove.common.service;

import cn.thylove.common.entity.MyPage;
import cn.thylove.common.entity.PageResult;
import cn.thylove.common.entity.Result;
import cn.thylove.common.entity.co.UserCO;
import cn.thylove.common.entity.po.UserPO;
import cn.thylove.common.entity.vo.UserVO;
import cn.thylove.common.model.sys.User;

/**
 * 用户表
 *
 * @author dsq 93787944@qq.com
 * @since 1.0.0 2024-01-27
 */
public interface UserService extends BaseService<User> {

    PageResult<UserVO> page(MyPage<UserCO> co);

    Result<UserVO> saveUser(UserPO po);
//
//    void save(SysUserVO vo);
//
//    void update(SysUserVO vo);
//
//    void delete(List<Long> idList);
}