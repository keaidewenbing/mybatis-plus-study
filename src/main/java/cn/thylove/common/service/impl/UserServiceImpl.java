package cn.thylove.common.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.thylove.common.entity.MyPage;
import cn.thylove.common.entity.PageResult;
import cn.thylove.common.entity.Result;
import cn.thylove.common.entity.co.UserCO;
import cn.thylove.common.entity.po.UserPO;
import cn.thylove.common.entity.vo.UserVO;
import cn.thylove.common.mapper.UserMapper;
import cn.thylove.common.model.sys.User;
import cn.thylove.common.service.UserService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 用户表
 *
 * @author dsq 93787944@qq.com
 * @since 1.0.0 2024-01-27
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<UserMapper, User> implements UserService {
    @Override
    public PageResult<UserVO> page(MyPage<UserCO> co) {
        Page<User> page = co.toMpPage();
        UserCO condition = co.getCondition();
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(StringUtils.isNotEmpty(condition.getUserName()), User::getUserName, condition.getUserName());
        userLambdaQueryWrapper.eq(StringUtils.isNotEmpty(condition.getSex()), User::getSex, condition.getSex());
        userLambdaQueryWrapper.eq(StringUtils.isNotEmpty(condition.getUserType()), User::getUserType, condition.getUserType());
        this.page(page, userLambdaQueryWrapper);
        return PageResult.of(page, UserVO.class);
    }

    @Override
    public Result<UserVO> saveUser(UserPO po){
        User user = BeanUtil.copyProperties(po, User.class);
        user.preInsert();
        this.save(user);
        return Result.data(BeanUtil.copyProperties(user, UserVO.class));
    }

//    @Override
//    public PageResult<SysUserVO> myPage(SysUserQuery query) {
//        IPage<SysUserEntity> myPage = baseMapper.selectPage(getPage(query), getWrapper(query));
//
//        return new PageResult<>(SysUserConvert.INSTANCE.convertList(myPage.getRecords()), myPage.getTotal());
//    }
//
//    private LambdaQueryWrapper<SysUserEntity> getWrapper(SysUserQuery query){
//        LambdaQueryWrapper<SysUserEntity> wrapper = Wrappers.lambdaQuery();
//        return wrapper;
//    }
//
//    @Override
//    public void save(SysUserVO vo) {
//        SysUserEntity entity = SysUserConvert.INSTANCE.convert(vo);
//
//        baseMapper.insert(entity);
//    }
//
//    @Override
//    public void update(SysUserVO vo) {
//        SysUserEntity entity = SysUserConvert.INSTANCE.convert(vo);
//
//        updateById(entity);
//    }
//
//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void delete(List<Long> idList) {
//        removeByIds(idList);
//    }

}