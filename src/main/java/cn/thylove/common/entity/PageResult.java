package cn.thylove.common.entity;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResult<T> implements Serializable {
    @Serial
    private static final long serialVersionUID = 9056411043515781783L;
    private long pageNum;
    private long pageSize;
    private long totalPage;
    private long total;
    private List<T> data;
    /**
     * 返回空的分页结果
     * @param page MyBatis Plus 的分页结果
     * @param <V> 目标 VO 类型
     * @param <P> 原始的 PO 类型
     * @return VO 的分页结果
     */
    public static <V, P> PageResult<V> empty(Page<P> page){
        long l = page.getTotal() / page.getSize();
        return new PageResult<>(page.getCurrent(), page.getSize(), (long) Math.ceil((double) l), page.getTotal(), Collections.emptyList());
    }
    /**
     * 将 MyBatis Plus 的分页结果转换为 VO 的分页结果
     * @param page MyBatis Plus 的分页结果
     * @param voClass 目标 VO 类型的字节码
     * @param <V> 目标 VO 类型
     * @param <P> 原始的 PO 类型
     * @return VO 的分页结果
     */
    public static <V, P> PageResult<V> of(Page<P> page, Class<V> voClass){
        // 非空校验
        List<P> records = page.getRecords();
        if(CollectionUtil.isEmpty(records)) {
            return empty(page);
        }
        // 数据转换
        List<V> vos = BeanUtil.copyToList(records, voClass);
        long l = page.getTotal() / page.getSize();
        // 封装返回
        return new PageResult<>(page.getCurrent(), page.getSize(), (long) Math.ceil((double) l), page.getTotal(), vos);
    }
    /**
     * 将 Mybatis Plus分页结果转为 VO分页结果，允许用户自定义 PO到 VO的转换方式
     * @param page Mybatis Plus的分页结果
     * @param convertor PO 到 VO的转换函数
     * @param <V> 目标 VO 类型
     * @param <P> 原始 PO 类型
     * @return VO 的分页结果
     */
    public static <V, P> PageResult<V> of(Page<P> page, Function<P, V> convertor){
        // 非空校验
        List<P> records = page.getRecords();
        if(CollectionUtil.isEmpty(records)) {
            return empty(page);
        }
        // 数据转换
        List<V> vos = records.stream().map(convertor).collect(Collectors.toList());
        long l = page.getTotal() / page.getSize();
        // 封装返回
        return new PageResult<>(page.getCurrent(), page.getSize(), (long) Math.ceil((double) l), page.getTotal(), vos);
    }
}