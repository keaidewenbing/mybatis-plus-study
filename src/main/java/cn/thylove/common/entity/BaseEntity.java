package cn.thylove.common.entity;

import cn.thylove.common.model.sys.User;
import cn.thylove.common.utils.UserUtils;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.nacos.shaded.com.google.common.collect.Lists;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.xml.bind.annotation.XmlTransient;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * @author: dsq
 * @data: 2024/1/28 14:00
 * @description: 基类
 */
public abstract class BaseEntity<T> implements Serializable {
  @Serial
  private static final long serialVersionUID = 1L;
  /**
   * 实体编号（唯一标识）
   */
  protected String id;

  /**
   * 当前用户
   */
  @TableField(exist = false)
  protected User currentUser;

  /**
   * 自定义SQL（SQL标识，SQL内容）
   */
  @TableField(exist = false)
  protected String dataScope;
  @TableField(exist = false)
  protected List<String> dateScopeList = Lists.newArrayList();

  /**
   * 是否是新记录（默认：false），调用setIsNewRecord()设置新记录，使用自定义ID。
   * 设置为true后强制执行插入语句，ID不会自动生成，需从手动传入。
   */
  @TableField(exist = false)
  protected boolean isNewRecord = false;

  /**
   * 主键策略（默认：UUID）
   */
  @TableField(exist = false)
  protected String IdType = IDTYPE_SNOWFLAKE;


  public BaseEntity() {
  }

  public BaseEntity(String id) {
    this();
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @JsonIgnore
  @JSONField(serialize = false)
  @XmlTransient
  public User getCurrentUser() {
    if(currentUser == null){
      currentUser = UserUtils.getUser();
    }
    return currentUser;
  }

  public void setCurrentUser(User currentUser) {
    this.currentUser = currentUser;
  }

  @JsonIgnore
  @JSONField(serialize = false)
  @XmlTransient
  public String getDataScope() {
    return dataScope;
  }

  public void setDataScope(String dataScope) {
    this.dataScope = dataScope;
  }


  @JsonIgnore
  @JSONField(serialize = false)
  @XmlTransient
  public List<String> getDateScopeList() {
    return dateScopeList;
  }

  public void setDateScopeList(List<String> dateScopeList) {
    this.dateScopeList = dateScopeList;
  }

  /**
   * 插入之前执行方法，子类实现
   */
  public abstract void preInsert() throws Exception;

  /**
   * 更新之前执行方法，子类实现
   */
  public abstract void preUpdate() throws Exception;

  /**
   * 是否是新记录（默认：false），调用setIsNewRecord()设置新记录，使用自定义ID。
   * 设置为true后强制执行插入语句，ID不会自动生成，需从手动传入。
   */
  @JsonIgnore
  @JSONField(serialize = false)
  public boolean getIsNewRecord() {
    return isNewRecord || StringUtils.isBlank(getId());
  }

  /**
   * 是否是新记录（默认：false），调用setIsNewRecord()设置新记录，使用自定义ID。
   * 设置为true后强制执行插入语句，ID不会自动生成，需从手动传入。
   */
  public void setIsNewRecord(boolean isNewRecord) {
    this.isNewRecord = isNewRecord;
  }



  @JsonIgnore
  @JSONField(serialize = false)
  public String getIdType() {
    return IdType;
  }

  public void setIdType(String idType) {
    IdType = idType;
  }

  @Override
  public boolean equals(Object obj) {
    if (null == obj) {
      return false;
    }
    if (this == obj) {
      return true;
    }
    if (!getClass().equals(obj.getClass())) {
      return false;
    }
    BaseEntity<?> that = (BaseEntity<?>) obj;
    return null != this.getId() && this.getId().equals(that.getId());
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this);
  }

  /**
   * 删除标记（0：正常；1：删除；2：审核；）
   */
  public static final String DEL_FLAG_NORMAL = "0";
  public static final String DEL_FLAG_DELETE = "1";
  public static final String DEL_FLAG_AUDIT = "2";

  public static final String IDTYPE_UUID = "UUID";
  public static final String  IDTYPE_AUTO = "AUTO";
  public static final String  IDTYPE_SNOWFLAKE = "SNOWFLAKE";

}
