package cn.thylove.common.entity;

import cn.thylove.common.model.sys.User;
import cn.thylove.common.utils.DateUtils;
import cn.thylove.common.utils.IdGen;
import cn.thylove.common.utils.UserUtils;

import java.io.Serial;
import java.util.Date;

public abstract class DataEntity<T> extends BaseEntity<T> {

    @Serial
    private static final long serialVersionUID = 1L;

    /**
     * 创建人
     */
    private String createBy;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 更新人
     */
    private String updateBy;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 删除标志（0 代表未删除 1 代表已删除）
     */
    private String delFlag;
    /**
     * 排序号
     */
    private String orderNo;

    /**
     *  备注
     */
    private String remarks;
    public DataEntity() {
        super();
        this.delFlag = DEL_FLAG_NORMAL;
    }

    public DataEntity(String id) {
        super(id);
    }

    /**
     * 插入之前执行方法，需要手动调用
     */
    @Override
    public void preInsert(){
        if (!this.isNewRecord){
            if (this.getIdType().equals(IDTYPE_SNOWFLAKE)) {
                setId(IdGen.nextId());
            } else if (this.getIdType().equals(IDTYPE_UUID)) {
                setId(IdGen.uuid());
            }  //使用自增长不需要设置主键

        }
        User user = UserUtils.getUser();
        this.createBy = user.getId();
        try {
            this.createTime = DateUtils.parseDate2String(new Date(), "yyyy-MM-dd HH:mm:ss");
        } catch (Exception e) {
            this.createTime = "";
        }
        this.updateBy = this.createBy;
        this.updateTime = this.createTime;
        setOrderNo(IdGen.nextId());
    }

    /**
     * 更新之前执行方法，需要手动调用
     */
    @Override
    public void preUpdate() throws Exception {
        User user = UserUtils.getUser();
        this.updateBy = user.getId();
        this.updateTime = DateUtils.parseDate2String(new Date(), "yyyy-MM-dd HH:mm:ss");
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }
}
