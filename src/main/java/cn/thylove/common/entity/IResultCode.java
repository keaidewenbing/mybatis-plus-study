package cn.thylove.common.entity;

import java.io.Serializable;

public interface IResultCode extends Serializable {

    String getMessage();

    int getCode();
}
