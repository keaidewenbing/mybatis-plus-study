package cn.thylove.common.entity.co;

/**
 * @author: dsq
 * @data: 2024/1/28 19:05
 * @description: User实体的查询条件
 */
public class UserCO {
    private String userName;

    private String sex;

    private String userType;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
}
