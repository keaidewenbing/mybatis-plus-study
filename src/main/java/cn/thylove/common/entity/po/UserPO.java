package cn.thylove.common.entity.po;

import lombok.Data;

/**
 * @author: dsq
 * @data: 2024/1/29 0:47
 * @description: TODO
 */
@Data
public class UserPO {
  /**
   * 用户名
   */
  private String userName;

  /**
   * 昵称
   */
  private String nickName;

  /**
   * 密码
   */
  private String password;

  /**
   * 账号状态（0正常 1停用）
   */
  private String status;

  /**
   * 邮箱
   */
  private String email;

  /**
   * 手机号
   */
  private String phoneNumber;

  /**
   * 用户性别（0男 1女 2未知）
   */
  private String sex;

  /**
   * 头像
   */
  private String avatar;
}
