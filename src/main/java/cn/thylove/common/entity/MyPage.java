package cn.thylove.common.entity;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serial;
import java.io.Serializable;
import java.util.Map;
import java.util.regex.Pattern;

public class MyPage<T> implements Serializable {

	@Serial
	private static final long serialVersionUID = 1L;
	/**
	 * 当前页码
	 */
	protected int pageNum = 1;
	/**
	 * 页面大小，设置为“-1”表示不进行分页（分页无效）
	 */
	protected int pageSize = 10;
	/**
	 * 总记录数，设置为“-1”表示不查询总数
	 */
	protected long total;
	/**
	 * 标准查询有效， 实例： update date desc, name asc
	 */
	private Map<String, Boolean> orderBy;

	private T condition;
	public MyPage() {
		this.pageSize = -1;
	}
	/**
	 * 构造方法
	 * @param pageNum 当前页码
	 * @param pageSize 分页大小
	 */
	public MyPage(int pageNum, int pageSize) {
		this(pageNum, pageSize, 0);
	}

	/**
	 * 构造方法
	 * @param pageNum 当前页码
	 * @param pageSize 分页大小
	 * @param total 数据条数
	 */
	public MyPage(int pageNum, int pageSize, long total) {
		this.setTotal(total);
		this.setPageNum(pageNum);
		this.pageSize = pageSize;
	}



	/**
	 * 获取当前页码
	 */
	public int getPageNum() {
		return pageNum;
	}

	/**
	 * 设置当前页码
	 */
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	/**
	 * 获取页面大小
	 */
	public int getPageSize() {
		return pageSize;
	}

	/**
	 * 设置页面大小
	 */
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize <= 0 ? 10 : pageSize;
	}
	/**
	 * 获取设置总数
	 */
	public long getTotal() {
		return total;
	}

	/**
	 * 设置数据总数
	 */
	public void setTotal(long total) {
		this.total = total;
		if (pageSize >= total){
			pageNum = 1;
		}
	}
	/**
	 * 获取查询排序字符串
	 */
	@JsonIgnore
	public String getOrderByField(String orderByField) {
		// SQL过滤，防止注入
		String reg = "(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"
				+ "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|total|master|into|drop|execute)\\b)";
		Pattern sqlPattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
		if (sqlPattern.matcher(orderByField).find()) {
			return "";
		}
		return orderByField;
	}

	public Map<String, Boolean> getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(Map<String, Boolean> orderBy) {
		this.orderBy = orderBy;
	}

	private void setCondition(T condition) {
		this.condition = condition;
	}

	public T getCondition() {
		return condition;
	}
	public <T> Page<T> toMpPage() {
		return this.toMpPage((OrderItem) null);
	}

	/**
	 * 将 MyPage 转化为 Mybatis Plus Page
	 *
	 * @param orderItems 手动设置排序条件
	 * @param <T>        泛型
	 * @return Mybatis Plus Page
	 */
	public <T> Page<T> toMpPage(OrderItem... orderItems) {
		// 1. 分页条件
		Page<T> p = Page.of(pageNum, pageSize);
		// 2. 排序提交
		if (CollectionUtil.isNotEmpty(orderBy)) {
			orderBy.forEach((k, v) -> p.addOrder(new OrderItem(getOrderByField(k), v)));

			return p;
		}

		if (orderItems != null) {
			p.addOrder(orderItems);
		}
		return p;
	}

	// 手动传入排序方式
	public <T> Page<T> toMpPage(String defaultSortBy, boolean isAsc) {
		return this.toMpPage(new OrderItem(defaultSortBy, isAsc));
	}

	// 默认 按照 CreateTime 降序排序
	public <T> Page<T> toMpPageDefaultSortByCreateTimeDesc() {
		return this.toMpPage("create_time", false);
	}

	// 默认 按照 UpdateTime 降序排序
	public <T> Page<T> toMpPageDefaultSortByUpdateTimeDesc() {
		return this.toMpPage("update_time", false);
	}


}