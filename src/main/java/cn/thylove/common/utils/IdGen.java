package cn.thylove.common.utils;

import cn.thylove.common.constant.SysConstant;

import java.util.UUID;

/**
 * @author: dsq
 * @data: 2024/1/28 17:50
 * @description: 封装各种生成唯一性ID算法的工具类
 */
public class IdGen {
    private static final SnowFlake snowFlake = new SnowFlake(SysConstant.DATA_CENTER_USER_ID, SysConstant.MACHINE_USER_ID);

    public static String nextId() {
        return String.valueOf(snowFlake.nextId());
    }

    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
